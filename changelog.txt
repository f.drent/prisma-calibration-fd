22-04-2024
* made MCP gate more narrow in the Y-direction.
* cath-left.cal & right-cath.cal are set to zero, so their events are not picked up.
* xfp-mm.cal calibrated better compared to my last calibration. The cathode sections should align much better now.
* updated the kinetic energy in binarypartner.conf
* updated the magn. fields in solver.conf and mass.conf

26-04-2024
* redid the aberration correction for Z=10 and Z=9. Still unsure about Z=9. For Z=10 looks much better!
* redid the charge gates z10q10-q9, z9q9-q8, z8q8

15-05-2024
* implemented the newly sent MCP gate and it looks good!
* redid aberration correction
* redid q-gates for Z=10
* mass spec looks good if one cuts off the fake/shadow blobs at X_FP{600,800}
